# Zedboard-test-2022.2

Vivado 2022.2 project to test Zedboard development board peripherals used in ICTP-MLAB workshops.

- USB Debugger
- USB Serial
- Ethernet
- AD1 PMOD
- DA1 PMOD


## How to use
Connect the PMODs as follows:
 - DA1 -> JA
 - AD1 -> JB
 
Interconnect both PMODs with a cable loopback (analog signal) this way:
 - DA1 [A1] -> AD1 [A0]

### Hardware
In Vivado, create a new project for ZedBoard. Do not forget to include the following source files:
- ComBlock IP in the IP repository
- HDL folder containing the VHDL drivers for ADC and DAC
- Constraints xdc file

Then, execute the block design TCL to replicate the hardware. Synthesize, implement and generate hardware. Export the hardware next.

### Firmware
In Vitis, create an Echo Server lwip FreeRTOS example, using the exported XSA hardware file from the previous step. Replace the main.c source file and leave the remaining source files intact.

### IP address configuration 
Ping or send an Echo request to the destination address:port. MAC Address is important to avoid packet collision in multiple device deployments within the same LAN.
    
    - IP: 140.105.17.145
    - Port: 7
    - MAC: 00:0A:35:00:01:12
